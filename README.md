<h1>Financial planning research.</h1>

<pre>
- If you aim for nothing you will hit it. 
- If it sounds like a deal that is too good to be true it probably is.
- Other people like your money as much as you do.
- Interview multiple people to pick an advisor.
- Pick a fiduciary.
- Don't be pressured to invest.
</pre>
<hr />
<pre>
<h1>LINKS: </h1>
Morningstar Investment Matrix
       VALUE        BLEND      GROWTH
       |----------|----------|----------|
LARGE  |          |          |          |
       |          |          |          |
       |----------|----------|----------|
MID    |          |          |          |
       |          |          |          |
       |----------|----------|----------|
SMALL  |          |          |          |
       |          |          |          |
       |----------|----------|----------|
       
When designing a portfolio try to diversify Mutual Funds in multiple boxes depending on your risk tolerance.


Equities = Stocks

Mutual Fund = Multiple Stocks in 1 fund investment.


Mutual Fund notes:

Index Fund compared to Managed Fund.

Index funds are based on stocks groups such as the S&P 500 and the Dow Jones Industrial Average while managed funds with selected stocks from determined by a fund manager. 

</pre>
# Create a portfolio that balances you risk tollerance. 
# https://www.morningstar.com/funds
# https://www.morningstar.com/investing-definitions
# https://www.investor.gov/financial-tools-calculators/calculators/compound-interest-calculator
GitLab.com
https://www.morningstar.com/stocks/xnas/gtlb/quote

Morningstar. You do not have to pay to create an account.
If you do create an account you will get emails.

<h1>MUTUAL FUNDS </h1>
(Groups of stocks.)

<h1>STOCKS </h1>
(Ownership in a company with dividend distributions.)

<h1>LIFE INSURANCE  </h1>
- TERM (You pay for what you get)
- WHOLE LIFE , UNIVERSAL LIFE AND MORE
    (You invest money and can evantually get the money back.)


<h1>401K or 403B</h1>
You do not pay TAX now but will when you collect you money.
(The IRS dictates you can withdraw funds from your 401(k) 
account without penalty only after you reach age 59.5, 
become permanently disabled, or are otherwise unable to work.)
(Manditory withdrawl at age 70)
In the United States, a 401(k) plan is an employer-sponsored, 
defined-contribution, personal pension (savings) account, 
as defined in subsection 401(k) of the U.S. Internal Revenue Code. 
Periodical employee contributions come directly out of their paychecks, 
and may be matched by the employer. 


<h1>Roth IRA (Traditional)</h1>
You pay TAX now and not on the growth.
A Roth IRA is an individual retirement account (IRA) under United States 
law that is generally not taxed upon distribution.
You can move money that you have invested with no penalty.


<h1>Roth 403B (Not available to all investors)</h1>
A Roth 403(b) plan is one type of tax-advantaged, 
employer-sponsored retirement savings account that 
combines elements of a Roth IRA and a traditional 403(b). 
While these plans share some similarities with 401(k) plans, 
they have certain characteristics that set them apart. 
If you have the option to save for retirement in a Roth 403(b) 
through your employer, this guide can help you understand how 
they work and what tax benefits they offer. For more hands-on 
guidance as you navigate the ins and outs of a Roth 403(b) plan, 
consider finding a financial advisor.

<h1> Fiduciary </h1>
A fiduciary is an individual or organization that acts in the 
best interest of a particular individual.



